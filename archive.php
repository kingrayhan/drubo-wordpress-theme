<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Drubo
 */
?>
<?php get_header(); global $drubo_theme_options; ?>
<section class="page-content ptb-100">
	<div class="container">
		<div class="row">

		
			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header>


			<?php echo get_template_part('template-parts/'.$drubo_theme_options['select_arc_sidebar_layout']); 
				if(empty($drubo_theme_options)) echo get_template_part('template-parts/blog-right-sidebar'); 
			?>
			



		</div>
	</div>
</section>
<?php get_footer(); ?>