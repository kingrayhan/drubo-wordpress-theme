<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Drubo
 */

if ( ! function_exists( 'drubo_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function drubo_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'drubo' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'drubo' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'drubo_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function drubo_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'drubo' ) );
		if ( $categories_list && drubo_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'drubo' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'drubo' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'drubo' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'drubo' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'drubo' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function drubo_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'drubo_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'drubo_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so drubo_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so drubo_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in drubo_categorized_blog.
 */
function drubo_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'drubo_categories' );
}
add_action( 'edit_category', 'drubo_category_transient_flusher' );
add_action( 'save_post',     'drubo_category_transient_flusher' );




function social_links_li(){ 
	global $drubo_theme_options;
	if(!empty($drubo_theme_options['social_icon_links'])):
?>	
	<?php foreach ($drubo_theme_options['social_icon_links'] as $key => $value) : ?>
	<?php if( $key == 'Twitter' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Facebook' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Google+' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Pinterest' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Linkedin' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Github' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-github" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Instagram' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Medium' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-medium" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Tumblr' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Youtube' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if($key == 'Vine' && !empty($value)) : ?>
		<li><a href="<?php echo $value; ?>"><i class="fa fa-vine" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php endforeach; endif;?>
<?php }


/**
 * Content meta
 */
function content_meta(){ 
global $drubo_theme_options;
?>
	<div class="bottom-article">
		<ul class="meta-post">
		  <?php if($drubo_theme_options['show_blog_date']): ?><li><i class="fa fa-calendar"></i><?php the_time( $drubo_theme_options['show_blog_date_format'] ); ?></li><?php endif; ?>

        <?php if($drubo_theme_options['show_blog_author']): ?><li><i class="fa fa-user"></i> <?php the_author_posts_link(); ?> </li><?php endif; ?>

        <?php if($drubo_theme_options['show_blog_comment_count']): ?><li><i class="fa fa-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number( $drubo_theme_options['blog_no_comment_text'] , $drubo_theme_options['blog_one_comment_text'] , $drubo_theme_options['blog_n_comment_text'] );?></a></li><?php endif; ?>
		</ul>
	</div>
<?php }


function title_and_meta(){ ?>
			<?php 
			$prefix = 'Drubo_';
			$show_title_and_meta = get_post_meta( get_the_id() , $prefix . 'show_title_and_meta' , true );
			if($show_title_and_meta): ?>
			<div class="post-meta-info">
				<h3><?php the_title(); ?></h3>
				<?php content_meta(); ?>	
			</div>
			<?php endif; ?>
<?php }



/**
 *  ======================================
 *      Topbar Left Elements
 *  ======================================
 */
// Contact info
function contact_info_left(){
	global $drubo_theme_options;
	$number = $drubo_theme_options['topbar_phone'];
	$mail = $drubo_theme_options['topbar_mail'];

	echo "<div class=\"header-left ptb-15\">";
		echo "<ul>";
			echo (!empty($number)) ? "<li><span><i class=\"zmdi zmdi-phone\"></i></span><a href=\"#\"> " .$number. " </a></li>" : "";
			echo (!empty($mail)) ? "<li><span><i class=\"zmdi zmdi-email\"></i></span><a href=\"#\"> " .$mail. " </a></li>" : "";
		echo "</ul>";
	echo "</div>";
}
// Social icons
function social_icons_left(){ 
	echo "<div class=\"social-icon pull-left\">";
		echo "<ul>";
			social_links_li();
		echo "</ul>";
	echo "</div>";
}
// Content
function content_left(){ 
	global $drubo_theme_options;
	$leftContent = $drubo_theme_options['top_left_content'];
	$leftContent = preg_replace('!^<p>(.*?)</p>$!i', '$1', $leftContent);

	echo "<p class='left-content pt-15'>".$leftContent."</p>";
}
// Left Menu
function LeftMenu(){  
		$args = array(
			'theme_location' => 'LeftMenu',
			'container' => false,
			'fallback_cb' => 'drubo_fb_menu',
			'items_wrap' => '<ul id = "%1$s" class = "topbarMenu pt-15 %2$s">%3$s</ul>',
		);
		wp_nav_menu( $args );
}
/**
 *  ======================================
 *      Topbar Left
 *  ======================================
 */
function topbarLeft(){ 
	global $drubo_theme_options;
	if($drubo_theme_options['top_bar_left_element'] == 1)
		social_icons_left();
	if($drubo_theme_options['top_bar_left_element'] == 2)
		LeftMenu();
	if($drubo_theme_options['top_bar_left_element'] == 3)
		contact_info_left();
	if($drubo_theme_options['top_bar_left_element'] == 4)
		content_left();
}


/**
 *  ======================================
 *      Topbar Right Elements
 *  ======================================
 */


function social_icons_right(){
	echo "<div class=\"social-icon pull-right\">";
		echo "<ul>";
			social_links_li();
		echo "</ul>";
	echo "</div>";
}
function RightMenu(){
		$args = array(
			'theme_location' => 'RightMenu',
			'container' => false,
			'fallback_cb' => 'drubo_fb_menu',
			'items_wrap' => '<ul id = "%1$s" class = "topbarMenu pull-right pt-15 %2$s">%3$s</ul>',
		);
		wp_nav_menu( $args );
}
function contact_info_right(){
	global $drubo_theme_options;
	$number = $drubo_theme_options['topbar_phone'];
	$mail = $drubo_theme_options['topbar_mail'];

	echo "<div class=\"header-right ptb-15 header-right-contact pull-right\">";
		echo "<ul>";
			echo (!empty($number)) ? "<li style='margin-right:35px;'><span><i class=\"zmdi zmdi-phone\"></i></span><a href=\"#\"> " .$number. " </a></li>" : "";
			echo (!empty($mail)) ? "<li><span><i class=\"zmdi zmdi-email\"></i></span><a href=\"#\"> " .$mail. " </a></li>" : "";
		echo "</ul>";
	echo "</div>";
}


function content_right(){ 
	global $drubo_theme_options;
	$leftContent = $drubo_theme_options['top_right_content'];
	$leftContent = preg_replace('!^<p>(.*?)</p>$!i', '$1', $leftContent);

	echo "<p class='right-content pt-15 text-right'>".$leftContent."</p>";
}



function social_icon_with_search_icon(){
	global $drubo_theme_options;
echo "<div class=\"header-right social-icon pull-right\">";
	echo "<ul>";
		social_links_li();
		if($drubo_theme_options['control_top_bar_search']):
		echo "<li class=\"search-active\">";
		   echo "<a href=\"#\"><i class=\"zmdi zmdi-search\"></i></a>";
			echo "<div class=\"header-search-form\">";
				echo "<form action=\"".home_url()."\" method=\"get\">";
					echo "<input name=\"s\" id=\"search\" placeholder=\"Search\" type=\"text\">";
				echo "</form>";
			echo "</div>";
		echo "</li>";
		endif;
	echo "</ul>";
echo "</div>";
}




function topbarRight(){
	global $drubo_theme_options;
	if($drubo_theme_options['top_bar_right_element'] == 1)
		social_icon_with_search_icon();
	if($drubo_theme_options['top_bar_right_element'] == 2)
		social_icons_right();
	if($drubo_theme_options['top_bar_right_element'] == 3)
		RightMenu();
	if($drubo_theme_options['top_bar_right_element'] == 4)
		contact_info_right();
	if($drubo_theme_options['top_bar_right_element'] == 5)
		content_right();
}






// pagination
if ( !function_exists('drubo_pagination')) {
	function drubo_pagination( $query = '' ){
		if(function_exists('wp_pagenavi')):
			echo "<div class=\"pagination-count\">";
				if(!empty($query))
					wp_pagenavi(array('query' => $query));
				else
					wp_pagenavi();
			echo "</div>";
		endif;
	}
}





function drubo_is_blog() {
	global  $post;
	$posttype = get_post_type($post );
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  );
}





function page_titlebar_and_breadcrumbs(){ 
	global $drubo_theme_options;
	$prefix = 'Drubo_';
	/**
	 * Titlebar
	 */
	$enable_titlebar = get_post_meta( get_the_id() , $prefix . 'enable_titlebar' , true );
	$titlebar_bg_color = get_post_meta( get_the_id() , $prefix . 'titlebar_bg_color' , true );
	$titlebar_bg_image = get_post_meta( get_the_id() , $prefix . 'titlebar_bg_image' , true );
	$titlebar_bg_text_color = get_post_meta( get_the_id() , $prefix . 'titlebar_bg_text_color' , true );

	/**
	 * BreadCrumbs
	 */
	$enable_breadcrumps = get_post_meta( get_the_id() , $prefix . 'enable_breadcrumps' , true );
	$breadcrumbs_bg_color = get_post_meta( get_the_id() , $prefix . 'breadcrumbs_bg_color' , true );
	$breadcrumbs_txt_color = get_post_meta( get_the_id() , $prefix . 'breadcrumbs_txt_color' , true );
	$breadcrumbs_active_color = get_post_meta( get_the_id() , $prefix . 'breadcrumbs_active_color' , true );
	$breadcrumbs_sep = get_post_meta( get_the_id() , $prefix . 'breadcrumbs_sep' , true );
	

	/**
	 * Custom css
	 */
	$page_css = get_post_meta( get_the_id() , 'page_css' , true );
	

	$style = ""; // style varibale
	
	if(!empty($titlebar_bg_color)) $style .= "#titlebar-page{background-color: $titlebar_bg_color;}";
	if(!empty($titlebar_bg_image)) $style .= "#titlebar-page{background-image: url($titlebar_bg_image);}";
	if(!empty($titlebar_bg_text_color)) $style .= "#title-page{color: $titlebar_bg_text_color;}";
	if(!empty($breadcrumbs_txt_color)) $style .= "#breadcrumbs-page a{color: $breadcrumbs_txt_color;}";
	if(!empty($breadcrumbs_active_color)) $style .= "#breadcrumbs-page .active{color: $breadcrumbs_active_color;}";
	
	/**
	 * Page breadcrumbs controll from metabox
	 */
	if(!empty($breadcrumbs_bg_color)) $style .= "#breadcrumbs-page{background-color: $breadcrumbs_bg_color;}";
	/**
	 * Custom css
	 */
	if(!empty($page_css)) $style .= $page_css;
	
	echo "<style>$style</style>"

?>
<?php if($enable_titlebar): ?>
		<!-- Banner Area Start -->
		<div class="banner-area pt-130 pb-120" id="titlebar-page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-page text-center text-white text-uppercase">
							<h1 id="title-page"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Banner Area End -->
<?php endif; ?>


<?php if( $enable_breadcrumps ): ?>
		<!-- Breadcrumb Area Start -->
		<div class="breadcrumb-area ptb-25 broder-bottom" id="breadcrumbs-page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="breadcrumb-list text-center text-uppercase">
							<ul>
								<li><a href="<?php echo esc_url( home_url() ); ?>"><?php echo esc_html__('HOME','drubo') ?></a><span class="divider"> <?php echo $breadcrumbs_sep; ?> </span></li>
								<li class="active"><?php the_title(); ?></li>
							</ul>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Breadcrumb Area End -->
<?php endif; ?>
<?php }



function drubo_page_content(){
	
	$prefix = 'Drubo_';
	$page_layout = get_post_meta( get_the_id() , $prefix . 'page_layout' , true );
	$page_type = get_post_meta( get_the_id() , $prefix . 'page_type' , true );
	$show_title_and_meta = get_post_meta( get_the_id() , $prefix . 'show_title_and_meta' , true );
	
	if($page_layout == 1)
		get_template_part('template-parts/content','no-sidebar');
	else if($page_layout == 2)
		get_template_part('template-parts/content','right-sidebar');
	else if($page_layout == 3)
		get_template_part('template-parts/content','left-sidebar');
	else if($page_layout == 4)
		get_template_part('template-parts/content','both-sidebar');
	else 
		get_template_part('template-parts/content','right-sidebar');
}


function drubo_slider_output(){
	$prefix = 'Drubo_';
	$slider_id = get_post_meta(get_the_id()  , $prefix . 'select_slider' , true );
	$enable_slider = get_post_meta(get_the_id()  , $prefix . 'enable_drubo_slider' , true );
	
	if($enable_slider == '1') echo do_shortcode('[drubo_slider slider_id='.$slider_id.']'); 

}