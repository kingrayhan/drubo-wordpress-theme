<?php
function ocdi_import_files() {
  return array(
    array(
      'import_file_name'           => 'Standard Demo',
      //'categories'                 => array( 'Category 1', 'Category 2' ),
      'import_file_url'            => get_template_directory_uri() . '/demo-contents/drubo-standard.xml',
      'import_widget_file_url'     => get_template_directory_uri() . '/demo-contents/drubo-standard-widget.wie',
      'import_customizer_file_url' => get_template_directory_uri() . '/demo-contents/drubo-standard-customiser.dat',
      'import_redux'               => array(
        array(
          'file_url'    => get_template_directory_uri() . '/demo-contents/redux.json',
          'option_name' => 'drubo_theme_options',
        ),
      ),
      'import_preview_image_url'   => get_template_directory_uri() . '/screenshot.png',
      //'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'your-textdomain' ),
    ),
    array(
      'import_file_name'           => 'Comming Soon',
      //'categories'                 => array( 'Category 1', 'Category 2' ),
      'import_file_url'            => get_template_directory_uri() . '/demo-contents/drubo-standard.xml',
      'import_widget_file_url'     => get_template_directory_uri() . '/demo-contents/drubo-standard-widget.wie',
      'import_customizer_file_url' => get_template_directory_uri() . '/demo-contents/drubo-standard-customiser.dat',
      'import_redux'               => array(
        array(
          'file_url'    => 'http://rayhan.info/wpdev/drubo/demo-contents/redux.json',
          'option_name' => 'drubo_theme_options',
        ),
      ),
      'import_preview_image_url'   => get_template_directory_uri() . '/inc/comming-soon.png',
      //'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'your-textdomain' ),
    ),


////////////////////////////////////////////////////////////////////
  );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );

add_action('admin_head',function(){
	?>
	<script>
		jQuery(document).ready(function($) {
			jQuery('button.ocdi__gl-item-button[value="1"]').remove()
		});
	</script>
	<?php
});

