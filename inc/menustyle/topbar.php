<!-- Header Top Area Start -->
<div class="header-top-area header-2">
   <div class="container">
		<div class="row">
			<!-- Header Top Left Start -->
			<div class="col-md-6 col-sm-7 col-xs-12">
			<?php 
				if($drubo_theme_options['control_top_bar_left'])
					topbarLeft(); 
			?>
			</div>
			<!-- Header Top Left End -->
			<!-- Header Top Right Start -->
			<div class="col-md-6 col-sm-5 col-xs-12">
					<?php 
					if($drubo_theme_options['control_top_bar_right'])
					topbarRight(); ?>
			</div>
			<!-- Header Top Right End -->
		</div>
   </div>
</div>
<!-- Header Top Area End -->