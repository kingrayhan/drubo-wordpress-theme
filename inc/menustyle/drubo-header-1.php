<?php global $drubo_theme_options; ?>
        <header class="header-wrapper">
        <?php if($drubo_theme_options['control_top_bar']): ?>
			<?php require dirname(__FILE__) . '/topbar.php'; ?>
		<?php endif; ?>
			<!-- Header Menu Area Start -->
			<div class="main-menu-area broder-top ptb-25" id="sticky-header">
				<div class="container">
					<div class="row">
						<!-- Logo Area -->
						<div class="col-md-2 col-sm-12 col-xs-12">
							<div class="logo-area">
								<?php drubo_logo(); ?>
							</div>
						</div>
						<!-- Logo Area -->
						<!-- Menu Area -->
						<div class="col-md-10 col-sm-12 hidden-xs">
							<div class="main-menu">
								<nav>
									<?php drubo_main_menu(); ?>
								</nav>
							</div>
						</div>
						<!-- Menu Area -->
					</div>
				</div>
				<!-- MOBILE-MENU-AREA START --> 
				<div class="mobile-menu-area">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12">
							<div class="mobile-area">
								<div class="mobile-menu">
									<nav id="mobile-nav">
										<?php drubo_mobile_menu(); ?>
									</nav>
								</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- MOBILE-MENU-AREA END  -->
			</div>
			<!-- Header Menu Area End -->
        </header>
        <!-- End of header area -->