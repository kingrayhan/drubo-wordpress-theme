<?php global $drubo_theme_options; ?>
        <header class="main-menu-area header-area-2 header-wrapper headroom">
			<!-- Header Top Area Start -->
			<div class="header-top-area">
				<div class="container">
					<div class="row">
						<!-- Logo Area -->
						<div class="col-md-2 col-sm-12">
							<div class="logo-area">
								<?php drubo_logo(); ?>
							</div>
						</div>
						<!-- Logo Area -->
						<!-- Menu Area -->
						<div class="col-md-<?php echo ($drubo_theme_options['header2_social_icon_control'] && count($drubo_theme_options['social_icon_links']) != 0) ? '8' : '10'; ?> col-sm-12 hidden-xs">
							<div class="main-menu">
								<nav>
									<?php drubo_main_menu(); ?>
								</nav>
							</div>
						</div>
						<!-- Menu Area -->
						<!-- MOBILE-MENU-AREA START --> 
						<div class="mobile-menu-area">
							<div class="container">
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<div class="mobile-area">
											<div class="mobile-menu">
												<nav id="mobile-nav">
													<?php drubo_mobile_menu(); ?>
												</nav>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- MOBILE-MENU-AREA END  -->
						<?php if($drubo_theme_options['header2_social_icon_control']): ?>
						<!-- Social Icon -->
						<div class="col-md-2 col-sm-12">
							<div class="social-rotate f-right mt-10">
									<ul>
										<?php social_links_li(); ?>
									</ul>
								</div>
							</div>
						<!-- Social Icon -->
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- Header Top Area End -->
        </header>
        <!-- End of header area -->


