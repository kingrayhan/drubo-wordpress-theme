<?php

add_action( 'cmb2_admin_init', 'drubo_page_metabox' );



function drubo_page_metabox() {

    global $drubo_theme_options;
    $prefix = 'Drubo_';

    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_page_titlebar_metabox',
        'title'         => esc_html__( 'Titlebar', 'drubo' ),
        'object_types'  => array( 'page', 'post'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
        'id' => $prefix .  'enable_titlebar',
        'name' => esc_html__('Enable Titlebar','drubo'),
        'type' => 'radio',
        'options'          => array(
            true    => esc_html__( 'Enable', 'drubo' ),
            false   => esc_html__( 'Disable', 'drubo' ),
        ),
        'default' => $drubo_theme_options['enable_page_titlebar']
    ));    
    $cmb->add_field(array(
        'id' => $prefix .  'titlebar_bg_color',
        'name' => esc_html__('Titlebar background color','drubo'),
        'type' => 'colorpicker',
        'default' => $drubo_theme_options['bg_titlebar_page']['background-color']
    ));        
    $cmb->add_field(array(
        'id' => $prefix .  'titlebar_bg_image',
        'name' => esc_html__('Titlebar background image','drubo'),
        'type' => 'file',
        'text'    => array(
            'add_upload_file_text' => 'Upload background image'
        ),
    ));    
    $cmb->add_field(array(
        'id' => $prefix .  'titlebar_bg_text_color',
        'name' => esc_html__('Titlebar title color','drubo'),
        'type' => 'colorpicker',
        'default' => $drubo_theme_options['page_titlebar_color']
    ));   
    ////////////////////////////////////////////////////////////////////

    /**
     * Breadcrumbs
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_page_tbreadcrumbs_metabox',
        'title'         => esc_html__( 'Breadcrumbs', 'drubo' ),
        'object_types'  => array( 'page', 'post'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
        'id' => $prefix .  'enable_breadcrumps',
        'name' => esc_html__('Enable Breadcrumbs','drubo'),
        'type' => 'radio',
        'options'          => array(
            true    => esc_html__( 'Enable', 'drubo' ),
            false   => esc_html__( 'Disable', 'drubo' ),
        ),
        'default' => $drubo_theme_options['enable_page_breadcrumbs'],
    ));    
    $cmb->add_field(array(
        'id'      => $prefix . 'breadcrumbs_bg_color',
        'name'    => esc_html__('Background color','drubo'),
        'type'    => 'colorpicker',
        'default' => $drubo_theme_options['bg_page_breadcrumbs'] 
    )); 
    $cmb->add_field(array(
        'id'      => $prefix . 'breadcrumbs_sep',
        'name'    => esc_html__('Breadcrumbs separators','drubo'),
        'type'    => 'text_small',
        'default' => $drubo_theme_options['page_breadcrumbs_separator'] 
    )); 
    $cmb->add_field(array(
        'id' => $prefix .  'breadcrumbs_txt_color',
        'name' => esc_html__('Text color','drubo'),
        'type' => 'colorpicker',
        'default' => $drubo_theme_options['page_breadcrumbs_text_color']
    ));    
    $cmb->add_field(array(
        'id' => $prefix .  'breadcrumbs_active_color',
        'name' => esc_html__('Active link color','drubo'),
        'type' => 'colorpicker',
        'default' => $drubo_theme_options['page_breadcrumbs_text_active_color']
    ));         
    ////////////////////////////////////////////////////////////////////

    /**
     * Page Layouts
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_page_layout',
        'title'         => esc_html__( 'Page Layout', 'drubo' ),
        'object_types'  => array( 'page', 'post'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
        'id' => $prefix .  'page_layout',
        'name' => esc_html__('Sidebar Layout','drubo'),
        'type' => 'radio',
        'options'          => array(
            1    => esc_html__( 'No Sidebar', 'drubo' ),
            2    => esc_html__( 'Right Sidebar', 'drubo' ),
            3    => esc_html__( 'Left Sidebar', 'drubo' ),
            4    => esc_html__( 'Both Sidebar', 'drubo' ),
        ),
        'default' => $drubo_theme_options['select_page_sidebar_layout'],
    ));  
    $cmb->add_field(array(
        'id' => $prefix .  'page_type',
        'name' => esc_html__('Page Layout','drubo'),
        'type' => 'radio',
        'options'          => array(
            'fullwidth'    => esc_html__( 'Full Width', 'drubo' ),
            'boxed'        => esc_html__( 'Boxed', 'drubo' ),
        ),
        'default' => $drubo_theme_options['drubo_page_layout'],
    ));   
    $cmb->add_field(array(
        'id' => $prefix . 'show_title_and_meta',
        'name' => esc_html__('Show Title and metas','drubo'),
        'type' => 'radio',
        'options'          => array(
            true    => esc_html__( 'show', 'drubo' ),
            false    => esc_html__( 'Hide', 'drubo' ),
        ),
        'default' => true,
    ));       

    ////////////////////////////////////////////////////////////////////

    /**
     * Custom css
     */
    
    $cmb = new_cmb2_box( array(
        'id'            => 'drubo_page_css',
        'title'         => esc_html__( 'Custom CSS', 'drubo' ),
        'object_types'  => array( 'page', 'post'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );
    ////////////////////////////////////////////////////////////////////

    $cmb->add_field(array(
        'id' => $prefix .  'page_css',
        'name' => esc_html__('Custom CSS','drubo'),
        'type' => 'textarea',
        'desc' => 'These code will only applicable for this page/post'
    ));  


    ////////////////////////////////////////////////////////////////////
}