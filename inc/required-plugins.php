<?php
add_action( 'tgmpa_register', 'drubo_required_plugin' );
function drubo_required_plugin() {
	$plugins = array(
		array(
			'name'      => esc_html__('Page Builder: KingComposer - Free Drag and Drop page builder by King-Theme','drubo'),
			'slug'      => 'kingcomposer',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('CMB2','drubo'),
			'slug'      => 'cmb2',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Redux Framework','drubo'),
			'slug'      => 'redux-framework',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('WP-PageNavi','drubo'),
			'slug'      => 'wp-pagenavi',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Drubo Shortcodes','drubo'),
			'slug'      => 'drubo-shortcodes',
			'source'      => 'https://bitbucket.org/kingrayhan/drubo-shortcodes-plugin/get/master.zip',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('Custom Sidebars','drubo'),
			'slug'      => 'custom-sidebars',
			'required'  => true,
		),
		array(
			'name'      => esc_html__('MailChimp for WordPress','drubo'),
			'slug'      => 'mailchimp-for-wp',
		),
		array(
			'name'      => esc_html__('One Click Demo Importer','drubo'),
			'slug'      => 'one-click-demo-import',
		),
		array(
			'name'      => esc_html__('Contact Form 7','drubo'),
			'slug'      => 'contact-form-7',
		),
		array(
			'name'      => esc_html__('Gallery Final Tiles Grid','drubo'),
			'slug'      => 'final-tiles-grid-gallery-lite',
		),
		array(
			'name'      => esc_html__('Drubo Portfolio','drubo'),
			'slug'      => 'drubo-portfolio',
			'source'      => 'https://bitbucket.org/kingrayhan/drubo-portfolio-plugin/get/master.zip',
		),
		array(
			'name'      => esc_html__('Drubo Slider','drubo'),
			'slug'      => 'drubo-slider',
			'source'      => 'https://bitbucket.org/kingrayhan/drubo-slider-plugin/get/master.zip',
		),

	);
	$config = array(
		'id'           => 'drubo_required_plugin',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'drubo-required-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

	);
	tgmpa( $plugins, $config );
}