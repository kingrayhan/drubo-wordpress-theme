<?php

/**
 * ======================================================================
 * Drubo Multipurpose WP Theme is developed by @KingRayhan
 * @url: rayhan.info , https://profiles.wordpress.org/kingrayhan/
 * @email: rayhan095@gmail.com
 * ======================================================================
 */ 


/**
 * Background Patters 
 */
$patters_bg = array();
for ($i= 1; $i <=22 ; $i++) { 
    $patters_bg['pattern'.$i] = array( 'img' => get_template_directory_uri() . '/assets/images/pattern/pattern'.$i.'.png');
}




// ======================================================================================================
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    

    /**
     * send default data to DB when redux is installed.
     */ 
    add_action('switch_theme', function() {
      Redux::init('drubo_theme_options');
    });



    // This is your option name where all the Redux data is stored.
    $opt_name = "drubo_theme_options";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'submenu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Theme Options', 'drubo' ),
        'page_title'           => esc_html__( 'Theme Options', 'drubo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // Add content after the form.
    $args['footer_text'] = ''; // Footer Text

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'drubo' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'drubo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'drubo' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'drubo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'drubo' );
    Redux::setHelpSidebar( $opt_name, $content );



    /**
     * Header
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Header', 'drubo' ),
        'id'               => 'header',
        'customizer_width' => '400px',
        'icon'             => 'el el-home',
    ));

    /**
     * Menubar style
     */
    Redux::setSection( $opt_name, array(
        'title' => 'Menubar Settings',
        'id' => 'menubar_style',
        'subsection' => true,
        'fields'           => array(
            /**
             * Header style
             */
            array(
                'id'       => 'header_style',
                'type'     => 'image_select',
                'title'    => esc_html__( 'Header Style', 'drubo' ),
                   'options'  => array(
                        '1'  => array(
                            'img'   => get_template_directory_uri() . '/inc/header-style-img/drubo-header-1.png'
                        ),
                        '2'  => array(
                            'img'   => get_template_directory_uri() . '/inc/header-style-img/drubo-header-2.png'
                        )

                    ),
                   'default' => '1'
            ),


            /**
             * Menu padding
             */
            
            array(
                'id'             => 'menubar_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'output'         => array('.main-menu-area'),
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Menubar padding', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '25px',
                    'padding-bottom' => '25px',
                ),
            ),

            /**
             * Menu item padding
             */
            
            array(
                'id'             => 'menubar_item_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'top' => false,
                'bottom' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Menubar item padding', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '40px',
                    'padding-bottom' => '40px',
                    'padding-left' => '15px',
                    'padding-right' => '15px',
                ),
                'output'    => array('.main-menu ul > li > a')
            ),

            /**
             * Menu dropdown width
             * 
             */
             // problem with output
            
            array(
                'id'            => 'menubar_dropdown_width',
                'type'          => 'slider',
                'title'         => esc_html__( 'Dropdown width', 'drubo' ),
                //'output'        => array('width' => '.main-menu ul li > ul'),
                'default'       => 225,
                'min'           => 0,
                'step'          => 1,
                'max'           => 500,
                'display_value' => 'text'
            ),

            /**
             * Dropdown item padding
             */
            array(
                'id'             => 'menubar_dropdown_item_padding',
                'type'           => 'spacing',
                'output'         => array('.main-menu ul li ul > li > a'),
                'mode'           => 'padding',
                'all'            => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Dropdown Item Padding', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '10px',
                    'padding-bottom' => '10px',
                    'padding-left' => '10px',
                    'padding-right' => '10px',
                ),
            ),

            array(
                'id' => 'header2_social_icon_control',
                'type' => 'switch',
                'title' => esc_html__('Social Icons','drubo'),
                'on'    => esc_html__('Show','drubo'),
                'off'    => esc_html__('Hide','drubo'),
                'default' => true,
                'required' => array('header_style','equals','2'),
            ),



            ///////////////////////
        )
    ));
    
    /**
     * Menubar typography
     */
    Redux::setSection( $opt_name, array(
        'id' => 'menubar_typography',
        'title' => esc_html__('Menu Typography','drubo'),
        'subsection' => true,
        'fields' => array(
            array(
                'id'          => 'menubar_bg',
                'type'        => 'color',
                'output'      => array('.main-menu-area'), 
                'title'       => esc_html__( 'Menubar Background', 'drubo' ),
                'default'     => '#FFFFFF',
                'output'      => array(
                    'background-color' => '.main-menu-area'
                )
            ),
            array(
                'id'          => 'stycky_menu_bg',
                'type'        => 'color',
                'output'      => array('.sticky.main-menu-area,.headroom--pinned'),
                'title'       => esc_html__( 'Sticky Menu Background', 'drubo' ),
                'default'     => '#FFFFFF',
                'output'      => array(
                    'background-color' => '.main-menu-area.sticky',
                    'background-color' => 'header.headroom--pinned',
                )
            ),
            array(
                'id'       => 'mainmenu_typography',
                'output'   => array('.main-menu ul > li > a ' , '.main-menu ul > li > a '),
                'type'     => 'typography',
                'title'    => esc_html__( 'Menu Typography', 'drubo' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#303030',
                    'font-size'   => '14px',
                    'font-family' => 'raleway',
                    'font-weight' => '700',
                    'line-height' => '50px',
                    'text-align'  => 'left'
                )
            ),
            array(
                'id'       => 'dropdown_menu_typography',
                'type'     => 'typography',
                'title'    => esc_html__( 'Dropdown Menu Typography', 'drubo' ),
                'google'   => true,
                'default'  => array(
                    'color'       => '#303030',
                    'font-size'   => '12px',
                    'font-family' => 'raleway',
                    'font-weight' => '700',
                    'line-height' => '12px',
                    'text-align'  => 'left'
                ),
                'output' => array('.main-menu ul li ul > li > a')
            ),
            array(
                'id'       => 'mainmenu_dropdown_panel_bg',
                'type'     => 'color',
                'title'    => esc_html__( 'Dropdown panel color', 'drubo' ),
                'default'  => '#FFFFFF',
                'output'   => array('background-color' => '.main-menu ul li > ul')

            ),
            array(
                'id'       => 'mainmenu_hover_and_active_font_color',
                'type'     => 'color',
                'title'    => esc_html__( 'Hover and active menu item Font Color', 'drubo' ),
                'default'  => '#14b1bb',
                'output'   => array('.main-menu ul li.current-menu-item a,.main-menu ul li:hover a' , '.main-menu ul li ul li a:hover')

            ),

            ////////////////
        )
    ));

    /**
     * Menu social icons
     */
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__('Top Bar', 'drubo'),
        'id'     => 'drubo_top_bar_settings',
        'desc'   => esc_html__('Controls your top bar settings from here.', 'drubo'),
        'subsection' => true,
        'fields' => array(
            array(
                'id'          => 'control_top_bar',
                'type'        => 'switch',
                'title'       => esc_html__('Top bar (Show/Hide)', 'drubo'),
                'subtitle'   => esc_html__( 'Show/Hide Top Bar.', 'drubo' ),
                'default'     => 1,
                'on'          => 'Show',
                'off'         => 'Hide'
            ),
            array(
                'id'          => 'top_bar_left_element',
                'type'        => 'select',
                'title'       => esc_html__('Top bar left element', 'drubo'),
                'required'    => array('control_top_bar', 'equals', 1),
                'subtitle'   => esc_html__('Controls the content that displays in the top left section.', 'drubo'),
                'options'  => array(
                    '1' => 'Social Icon',
                    '2' => 'Left Menu',
                    '3' => 'Contact Info',
                    '4' => 'Content',
                    '5' => 'Leave Empty',
                ),
                'default'  => '3',
            ),
            array(
                'id'          => 'top_bar_right_element',
                'type'        => 'select',
                'title'       => esc_html__('Top bar Right element', 'drubo'),
                'required'    => array('control_top_bar', 'equals', 1),
                'subtitle'   => esc_html__('Controls the content that displays in the top right section.', 'drubo'),
                'options'  => array(
                    '1' => 'Social Icon With Search Form',
                    '2' => 'Social Icon',
                    '3' => 'Right Menu',
                    '4' => 'Contact Info',
                    '5' => 'Content',
                    '6' => 'Leave Empty',
                ),
                'default'  => '1',
            ),
            array(
                'id'          => 'topbar_phone',
                'type'        => 'text',
                'required'    => array('control_top_bar', 'equals', 1),
                'title'       => esc_html__('Phone Number for Contact Info', 'drubo'),
                'subtitle'   => esc_html__('This content will display if you have "Contact Info" selected.', 'drubo'),
                'default'     => '(+880) 0123456789'
            ),
            array(
                'id'          => 'topbar_mail',
                'type'        => 'text',
                'required'    => array('control_top_bar', 'equals', 1),
                'title'       => esc_html__('Email Address for Contact Info', 'drubo'),
                'subtitle'   => esc_html__('This content will display if you have "Contact Info" selected.', 'drubo'),
                'default'     => 'admin@gmail.com'
            ),
            array(
                'id'          => 'top_left_content',
                'type'        => 'editor',
                'required'    => array('control_top_bar', 'equals', 1),
                'title'       => esc_html__('Left Content', 'drubo'),
                'subtitle'   => esc_html__('This content will display if you have "Content" selected. form top bar left element', 'drubo'),
                'args'        => array(
                    'wpautop' => false,
                    'teeny'   => false
                ),
                'default'     => ''
            ),
            array(
                'id'          => 'top_right_content',
                'type'        => 'editor',
                'required'    => array('control_top_bar', 'equals', 1),
                'title'       => esc_html__('Right Content', 'drubo'),
                'subtitle'   => esc_html__('This content will display if you have "Content" selected. from top bar right element ', 'drubo'),
                'args'        => array(
                    'wpautop' => false,
                    'teeny'   => false
                ),
                'default'     => ''
            ),
            
            array(
                'id'          => 'control_top_bar_left',
                'type'        => 'switch',
                'title'       => esc_html__('Top bar left (Show/Hide)', 'drubo'),
                'required'    => array('control_top_bar', 'equals', 1),
                'subtitle'   => esc_html__( 'Show/Hide Top Bar left.', 'drubo' ),
                'default'     => 1,
                'on'          => 'Show',
                'off'         => 'Hide'
            ),
           
            array(
                'id'          => 'control_top_bar_right',
                'type'        => 'switch',
                'title'       => esc_html__('Top bar right (Show/Hide)', 'drubo'),
                'required'   => array('control_top_bar', 'equals', 1),
                'subtitle'   => esc_html__( 'Show/Hide Top Bar right.', 'drubo' ),
                'default'     => 1,
                'on'          => 'Show',
                'off'         => 'Hide'
            ),
            array(
                'id'          => 'control_top_bar_search',
                'type'        => 'switch',
                'title'       => esc_html__('Top bar right search form (Show/Hide)', 'drubo'),
                'required'   => array('control_top_bar_right', 'equals', 1),
                'subtitle'   => esc_html__( 'Show/Hide Top Bar Right search form.', 'drubo' ),
                'default'     => 1,
                'on'          => 'Show',
                'off'         => 'Hide'
            ),


            array(
                'id'    => 'top_bar_bg_color',
                'type'  => 'color',
                'transparent' => false,
                'title' => esc_html__( 'Top Bar Background Color', 'drubo' ),
                'subtitle' => esc_html__( 'Pick a color for the top bar background. ( default: #F5F5F5 )', 'drubo' ),
                'required'   => array('control_top_bar_right', 'equals', 1),
                'default'     => '#F5F5F5',
                'output'   => array('background-color' => '.header-top-area.header-2')
            ),
            array(
                'id'    => 'top_bar_text_color',
                'type'  => 'color',
                'transparent' => false,
                'title' => esc_html__( 'Top Bar Font Color', 'drubo' ),
                'subtitle' => esc_html__( 'Pick a color for the top bar text color. ( default: #606060 )', 'drubo' ),
                'default'     => '#606060',
                'required'   => array('control_top_bar_right', 'equals', 1),
                'output'   => array('.header-top-area.header-2')
            ),
            array(
                'id'    => 'top_bar_link_color',
                'type'  => 'color',
                'transparent' => false,
                'title' => esc_html__( 'Top Bar Link Color', 'drubo' ),
                'subtitle' => esc_html__( 'Pick a color for the top bar link color. ( default: #606060 )', 'drubo' ),
                'default'     => '#606060',
                'required'   => array('control_top_bar_right', 'equals', 1),
                'output'   => array('.header-top-area.header-2 a')
            ),
            array(
                'id'    => 'top_bar_link_hover_color',
                'type'  => 'color',
                'transparent' => false,
                'title' => esc_html__( 'Top Bar Link Hover Color', 'drubo' ),
                'subtitle' => esc_html__( 'Pick a color for the top bar link hover color. ( default: #14b1bb )', 'drubo' ),
                'default'     => '#14b1bb',
                'required'   => array('control_top_bar_right', 'equals', 1),
                'output'   => array('.header-top-area.header-2 a:hover')
            ),



        )
    ));

    /**
     * Logo
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Logo', 'drubo' ),
        'id'               => 'logo-section',
        'icon'             => 'el el-plus-sign',
        'fields'           =>array(
            array(
                'id'       => 'website_logo_or_title',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Use logo as', 'drubo' ),
                'options'  => array(
                    '1' => 'Image',
                    '2' => 'Text'
                ),
                'default'  => '1'
            ),

            
            array(
                'id'       => 'site_logo',
                'type'     => 'media',
                'title'    => esc_html__( 'Logo', 'drubo' ),
                'subtitle' => esc_html__( 'Upload website logo. Logo size should be 133px x 47px', 'drubo' ),
                'default' => array( 'url' => get_template_directory_uri() . '/assets/images/logo/1.png' ),
                'required' => array('website_logo_or_title' , 'equals' , '1')
            ),
            // Title
            array(
                'id' => 'website_title',
                'type' => 'text',
                'title' => esc_html__('Website Title','drubo'),
                'default' => get_bloginfo('name'),
                'required' => array('website_logo_or_title' , 'equals' , '2')
            ),
        )
    ));
    /**
     * Layout
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Layout', 'drubo' ),
        'id'               => 'layout',
        'icon'             => 'el el-website',
        'fields'           => array(
            array(
                'id'    => 'drubo_page_layout',
                'type'  => 'button_set',
                'title' => esc_html__('Select Page layout','drubo'),
                'desc'  => esc_html__('Controls the site layout.','drubo'),
                'options' => array(
                    'fullwidth' => 'Full Width', 
                    'boxed'     => 'Boxed', 
                 ), 
                'default' => 'fullwidth'
            ),
            array(
                'id'             => 'page_content_paddings',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Page Content Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '55px',
                    'padding-bottom' => '55px',
                ),
                'output' => array('.page-content')
            ),
            array(
                'id'    => 'main_content_bg',
                'type'  => 'background',
                'title' => esc_html__('Main content Background','drubo'),
                'subtitle'  => esc_html__('Controls the background of the main content area which is everything below header and above footer.','drubo'),
                'default'  => array(
                    'background-color' => '#FFFFFF',
                    'background-repeat' => 'no-repeat',
                    'background-size' => 'inherit',
                    'background-attachment' => 'scroll',
                    'background-position' => 'left top',
                ),
                'output' => array('.page-content')
            ),
            array(
                'id'    => 'main_content_outer_bg',
                'type'  => 'background',
                'title' => esc_html__('Main content outer background','drubo'),
                'subtitle'  => esc_html__('Controls the background of the outer background area in boxed mode.','drubo'),
                'required' => array(
                    array('drubo_page_layout','equals','boxed'),
                    array('use_predefine_patterns','equals','0'),
                ),
                'default'  => array(
                    'background-color' => '#f2f2f2',
                    'background-repeat' => 'no-repeat',
                    'background-size' => 'inherit',
                    'background-attachment' => 'scroll',
                    'background-position' => 'left top',
                ),
            ),
            array(
                'id'      => 'use_predefine_patterns',
                'type'    => 'switch',
                'title'   => esc_html__('Use pre defined patterns? ', 'drubo'),
                'on'      => esc_html__('Yes','drubo'),
                'off'      => esc_html__('No','drubo'),
                'required' => array('drubo_page_layout','equals','boxed'),
                'default' => false
            ),
            array(
                'id'    => 'main_content_outer_bg_image',
                'type'  => 'image_select',
                'title' => esc_html__('Pre defined outer background images','drubo'),
                'required' => array(
                    array('drubo_page_layout','equals','boxed'),
                    array('use_predefine_patterns','equals','1'),
                ),
                'options' => $patters_bg // defined in very top :)
            )
        /////////
        )
    ));


    /**
     * Typography section
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Typography', 'drubo' ),
        'id'               => 'typography',
        'icon'             => 'el el-fontsize',
    ));
    // Body typography
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Body typography', 'drubo' ),
        'id'               => 'body-typography-section',
        'subsection'       => true,
        'fields'           =>array(
            array(
                'id'        =>    'body_typography',
                'title'     =>  esc_html__('Body Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all body text.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '400',
                    'text-align'  => 'inherit',
                    'font-size'   => '14px',
                    'line-height' => '22px',
                    'color'       => '#606060',
                ),
                'output' => array('body' , 'body p')
            ),
            array(
                'id'        => 'body_link_color',
                'title'     => esc_html__('Link Color','drubo'),
                'subtitle'  => 'Controls the color of all text links.',
                'type'      => 'color',
                'default'   => '#606060',
                'output'    => array('body a')
            ),
            array(
                'id'        => 'body_link_color_hover',
                'title'     => esc_html__('Link Hover Color','drubo'),
                'subtitle'  => 'Controls the color of all text links hover.',
                'type'      => 'color',
                'default'   => '#14B1BB',
                'output'    => array('body a:hover')
            ),
        )
    ));
    // Headers typography
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Headers typography', 'drubo' ),
        'id'               => 'headers-typography',
        'subsection'       => true,
        'fields'           =>array(
            array(
                'id'        =>    'h1_typography',
                'title'     =>  esc_html__('H1 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H1 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '700',
                    'text-align'  => 'inherit',
                    'font-size'   => '36px',
                    'line-height' => '45px',
                    'color'       => '#606060',
                ),
                'output' => array('h1')
            ),
            array(
                'id'        =>    'h2_typography',
                'title'     =>  esc_html__('H2 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H1 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '500',
                    'text-align'  => 'inherit',
                    'font-size'   => '30px',
                    'line-height' => '45px',
                    'color'       => '#606060',
                ),
                'output' => array('h2')
            ),
            array(
                'id'        =>    'h3_typography',
                'title'     =>  esc_html__('H3 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H3 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '500',
                    'text-align'  => 'inherit',
                    'font-size'   => '24px',
                    'line-height' => '45px',
                    'color'       => '#606060',
                ),
                'output' => array('h3')
            ),
            array(
                'id'        =>    'h4_typography',
                'title'     =>  esc_html__('H4 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H4 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '400',
                    'text-align'  => 'inherit',
                    'font-size'   => '18px',
                    'line-height' => '35px',
                    'color'       => '#606060',
                ),
                'output' => array('h4')
            ),
            array(
                'id'        =>    'h5_typography',
                'title'     =>  esc_html__('H5 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H5 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '400',
                    'text-align'  => 'inherit',
                    'font-size'   => '14px',
                    'line-height' => '30px',
                    'color'       => '#606060',
                ),
                'output' => array('h5')
            ),
            array(
                'id'        =>    'h6_typography',
                'title'     =>  esc_html__('H6 Headers Typography','drubo'),
                'subtitle'  =>  esc_html__('These settings control the typography for all H6 Headers.','drubo'),
                'type'      => 'typography',
                'subsets'   => false,
                'default'   => array(
                    'font-family' => 'Raleway',
                    'font-weight' => '400',
                    'text-align'  => 'inherit',
                    'font-size'   => '12px',
                    'line-height' => '30px',
                    'color'       => '#606060',
                ),
                'output' => array('h6')
            ),

        )
    ));


    // Titlebar & Breadcrumbs
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Titlebar & Breadcrumbs', 'drubo' ),
        'id'               => 'breadcrumbs',
        'icon'             => 'el el-adjust-alt',
    ));
    // Titlebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Titlebar', 'drubo' ),
        'id'               => 'page-breadcrumbs',
        'customizer_width' => '400px',
        'subsection'       => true,
        'fields' => array(
            array(
                'id' => 'enable_page_titlebar',
                'type' => 'switch',
                'title' => esc_html__('Enable Titlebar','drubo'),
                'on'    => esc_html__('Enable','drubo'),
                'off'    => esc_html__('Disable','drubo'),
                'default' => true
            ),
            array(
                'id' => 'bg_titlebar_page',
                'type' => 'background',
                'title' => esc_html__('Titlebar Background','drubo'),
                'required' => array('enable_page_titlebar','equals','1'),
                'default'  => array(
                    'background-color' => '#14b1bb',
                    'background-repeat' => 'no-repeat',
                    'background-size' => 'inherit',
                    'background-attachment' => 'scroll',
                    'background-position' => 'left top',
                ),
                'output' => array('#titlebar-page')
            ),
            array(
                'id' => 'page_titlebar_color',
                'type' => 'color',
                'title' => esc_html__('Title color','drubo'),
                'required' => array('enable_page_titlebar','equals','1'),
                'default' => '#FFFFFF'
            ),
            array(
                'id'             => 'page_titlebar_margin',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Margins', 'drubo' ),
                'default'        => array(
                    'margin-top'    => '0px',
                    'margin-bottom' => '0px',
                ),
                'required' => array('enable_page_titlebar','equals','1'),
                'output' => array('#titlebar-page')
            ),
            array(
                'id'             => 'page_titlebar_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '130px',
                    'padding-bottom' => '120px',
                ),
                'required' => array('enable_page_titlebar','equals','1'),
                'output' => array('#titlebar-page')
            )
        )
    ));
    // Breadcrumbs
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Breadcrumbs', 'drubo' ),
        'id'               => 'post-breadcrumbs',
        'subsection'       => true,
        'fields' => array(

            array(
                'id' => 'enable_page_breadcrumbs',
                'type' => 'switch',
                'title' => esc_html__('Enable Titlebar','drubo'),
                'on'    => esc_html__('Enable','drubo'),
                'off'    => esc_html__('Disable','drubo'),
                'default' => true
            ),
            array(
                'id'             => 'page_breadcrumbs_separator',
                'type'           => 'text',
                'title'          => esc_html__( 'Breadcrumbs separators', 'drubo' ),
                'default'        => '//',
                'required' => array('enable_page_breadcrumbs','equals','1'),
            ),

            array(
                'id' => 'bg_page_breadcrumbs',
                'type' => 'color',
                'title' => esc_html__('Background Color','drubo'),
                 'required' => array('enable_page_breadcrumbs','equals','1'),
                'default' => '#FFFFFF'
            ),
            array(
                'id' => 'page_breadcrumbs_text_color',
                'type' => 'color',
                'title' => esc_html__('Text Color','drubo'),
                 'required' => array('enable_page_breadcrumbs','equals','1'),
                'default' => '#383838'
            ),
            array(
                'id' => 'page_breadcrumbs_text_active_color',
                'type' => 'color',
                'title' => esc_html__('Active link color','drubo'),
                 'required' => array('enable_page_breadcrumbs','equals','1'),
                'default' => '#383838'
            ),
            array(
                'id'             => 'page_breadcrums_margin',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'all'            => false,
                'left' => false,
                'right' => false,

                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Margins', 'drubo' ),
                'default'        => array(
                    'margin-top'    => '0px',
                    'margin-bottom' => '0px',
                ),
                'required' => array('enable_page_breadcrumbs','equals','1'),
                'output' => array('#breadcrumbs-page')
            ),
            array(
                'id'             => 'page_breadcrumb_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '25px',
                    'padding-bottom' => '25px',
                ),
                'required' => array('enable_page_breadcrumbs','equals','1'),
                'output' => array('#breadcrumbs-page')
            ),

        )
    ));
    

    /**
     * Blog
     */

    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Blog', 'drubo' ),
        'id'               => 'blog',
        'icon'             => 'el el-file-edit',
        'fields'           => array(
            array(
                'id'    => 'blog_content_display',
                'title' => esc_html__('Blog Content Display','drubo'),
                'type'  => 'button_set',
                'options' => array(
                    'excerpt' => 'excerpt',
                    'Full_Content'  => 'Full Content'
                ),
                'default'   => 'excerpt'
            ),
            array(
                'id'            => 'blog_excerpt_length',
                'type'          => 'slider',
                'title'         => esc_html__( 'Blog excerpt length', 'drubo' ),
                'default'       => 40,
                'min'           => 0,
                'step'          => 5,
                'max'           => 300,
                'display_value' => 'text',
                'required' => array('blog_content_display','equals','excerpt')
            ),
            array(
                'id'            => 'blog_meta_section',
                'type'          => 'section',
                'title'         => esc_html__( 'Blog Metas', 'drubo' ),
                'indent'        => true
            ),
            array(
                'id'            => 'show_blog_date',
                'type'          => 'switch',
                'title'         => esc_html__( 'Show Date', 'drubo' ),
                'default'       => true
            ),
            array(
                'id'            => 'show_blog_date_format',
                'type'          => 'text',
                'title'         => esc_html__( 'Date Format', 'drubo' ),
                'desc'          => esc_html__('Control the date format. Here is <a href="http://codex.wordpress.org/Formatting_Date_and_Time">Formatting Date and Time</a>','drubo'),
                'default'       => 'M d, Y',
                'required'      => array('show_blog_date','equals',1)
            ),
            array(
                'id'            => 'show_blog_author',
                'type'          => 'switch',
                'title'         => esc_html__( 'Show Author', 'drubo' ),
                'default'       => true
            ),
            array(
                'id'            => 'show_blog_comment_count',
                'type'          => 'switch',
                'title'         => esc_html__( 'Show comment count', 'drubo' ),
                'default'       => true
            ),
            array(
                'id'            => 'blog_no_comment_text',
                'type'          => 'text',
                'title'         => esc_html__( 'Zero comment text', 'drubo' ),
                'subtitle'      => esc_html__('When there is no comment on the post' , 'drubo'),
                'default'       => 'No Comment',
                'required'      => array('show_blog_comment_count','equals',1)
            ),
            array(
                'id'            => 'blog_one_comment_text',
                'type'          => 'text',
                'title'         => esc_html__( 'One comment text', 'drubo' ),
                'subtitle'      => esc_html__('When there is one comment on the post' , 'drubo'),
                'default'       => '1 Comment',
                'required'      => array('show_blog_comment_count','equals',1)
            ),
            array(
                'id'            => 'blog_n_comment_text',
                'type'          => 'text',
                'title'         => esc_html__( 'N comment text', 'drubo' ),
                'subtitle'      => esc_html__('When there is number of comments on the post' , 'drubo'),
                'default'       => '% Comment',
                'required'      => array('show_blog_comment_count','equals',1)
            ),
            array(
                'id'            => 'show_blog_readmore',
                'type'          => 'switch',
                'title'         => esc_html__( 'Show read more', 'drubo' ),
                'default'       => true
            ),
            array(
                'id'    => 'blog_read_more_txt',
                'title' => esc_html__('Blog Read More text','drubo'),
                'type'  => 'text',
                'default'   => 'Continue reading ...',
                'required'  => array('show_blog_readmore','equals',1)
            ),
        )
    ));
    // Sidebar section
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Sidebar', 'drubo' ),
        'id'               => 'sidebar',
        'icon'             => 'el el-website',
    ));
    // Blog sidebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Blog Sidebar', 'drubo' ),
        'id'               => 'blog-sidebar-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'select_blog_left_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Left Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'left-sidebar',
            ),
            array(
                'id'       => 'select_blog_right_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Right Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'right-sidebar',
            ),
            array(
                'id'       => 'select_blog_sidebar_layout',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Sidebar Layout', 'drubo' ),
                'options'  => array(
                    'blog-no-sidebar'     => esc_html__( 'No Sidebar', 'drubo' ),
                    'blog-right-sidebar'  => esc_html__( 'Right Sidebar', 'drubo' ),
                    'blog-left-sidebar'   => esc_html__( 'Left Sidebar', 'drubo' ),
                    'blog-both-sidebar'   => esc_html__( 'Both Sidebar', 'drubo' ),
                ),
                'default' => 'blog-right-sidebar',
            ),
        )
    ));
    // Archieve page sidebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Archieve Sidebar', 'drubo' ),
        'id'               => 'arc-sidebar-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'select_arc_left_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Left Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'left-sidebar',
            ),
            array(
                'id'       => 'select_arc_right_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Right Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'right-sidebar',
            ),
            array(
                'id'       => 'select_arc_sidebar_layout',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Sidebar Layout', 'drubo' ),
                'options'  => array(
                    'blog-no-sidebar'     => esc_html__( 'No Sidebar', 'drubo' ),
                    'blog-right-sidebar'  => esc_html__( 'Right Sidebar', 'drubo' ),
                    'blog-left-sidebar'   => esc_html__( 'Left Sidebar', 'drubo' ),
                    'blog-both-sidebar'   => esc_html__( 'Both Sidebar', 'drubo' ),
                ),
                'default' => 'blog-right-sidebar',
            ),
        )
    ));
    // Search page sidebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Search Page Sidebar', 'drubo' ),
        'id'               => 'search-sidebar-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'select_search_left_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Left Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'left-sidebar',
            ),
            array(
                'id'       => 'select_search_right_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Right Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'right-sidebar',
            ),
            array(
                'id'       => 'select_search_sidebar_layout',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Sidebar Layout', 'drubo' ),
                'options'  => array(
                    'blog-no-sidebar'     => esc_html__( 'No Sidebar', 'drubo' ),
                    'blog-right-sidebar'  => esc_html__( 'Right Sidebar', 'drubo' ),
                    'blog-left-sidebar'   => esc_html__( 'Left Sidebar', 'drubo' ),
                    'blog-both-sidebar'   => esc_html__( 'Both Sidebar', 'drubo' ),
                ),
                'default' => 'blog-right-sidebar',
            ),
        )
    ));
    // Page sidebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Page Sidebar', 'drubo' ),
        'id'               => 'page-sidebar-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'select_page_left_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Left Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'left-sidebar',
            ),
            array(
                'id'       => 'select_page_right_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Right Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'right-sidebar',
            ),
            array(
                'id'       => 'select_page_sidebar_layout',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Sidebar Layout', 'drubo' ),
                'options'  => array(
                    1    => esc_html__( 'No Sidebar', 'drubo' ),
                    2    => esc_html__( 'Right Sidebar', 'drubo' ),
                    3    => esc_html__( 'Left Sidebar', 'drubo' ),
                    4    => esc_html__( 'Both Sidebar', 'drubo' ),
                ),
                'default' => 1,
            ),
        )
    ));
    // Post Sidebar
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Post Sidebar', 'drubo' ),
        'id'               => 'post-sidebar-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'select_post_left_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Left Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'left-sidebar',
            ),
            array(
                'id'       => 'select_post_right_sidebar',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Right Sidebar', 'drubo' ),
                'data'     => 'sidebar',
                'default'  => 'right-sidebar',
            ),
            array(
                'id'       => 'select_post_sidebar_layout',
                'type'     => 'select',
                'title'    => esc_html__( 'Select Sidebar Layout', 'drubo' ),
                'options'  => array(
                    1    => esc_html__( 'No Sidebar', 'drubo' ),
                    2    => esc_html__( 'Right Sidebar', 'drubo' ),
                    3    => esc_html__( 'Left Sidebar', 'drubo' ),
                    4    => esc_html__( 'Both Sidebar', 'drubo' ),
                ),
                'default' => 2,
            ),
        )
    ));
    // Sidebar Typography
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Sidebar Typography', 'drubo' ),
        'id'               => 'sidebar-typography-section',
        'subsection'       => true,
        'fields' => array(
            array(
                'id'       => 'sidebar_background_color',
                'type'     => 'color',
                'title'    => esc_html__( 'Background', 'drubo' ),
                'default'  => 'transparent',
                'output'   => array('background-color' => '.drubo-sidebar')
            ),
            array(
                'id' => 'sidebar_widget_spacing_section',
                'type' => 'section',
                'title' => esc_html__('Single widget spacing','drubo'),
                'indent' => true
            ),
            array(
                'id'             => 'sidebar_widget_paddings',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'left' => false,
                'right' => false,
                'default'        => array(
                    'padding-top' => '0px',
                    'padding-bottom' => '30px',
                ),
                'output' => array('.drubo-sidebar .widget')
            ),
            array(
                'id'             => 'sidebar_widget_margins',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'left' => false,
                'right' => false,
                'title'          => esc_html__( 'Margins', 'drubo' ),
                'default' => array(
                    'margin-top' => '0px',
                    'margin-bottom' => '5px'
                ),
                'output' => array('.drubo-sidebar .widget')
            ),

            array(
                'id' => 'sidebar_widget_title_typography',
                'type' => 'section',
                'title' => esc_html__('Widget Title','drubo'),
                'indent' => true
            ),

            array(
                'id' => 'sidebar_widget_title_coloR',
                'type' => 'color',
                'title' => esc_html__('Color','drubo'),
                'default' => '#383838',
                'output' => array('.drubo-sidebar .widgetheading')
            ),
            array(
                'id'             => 'sidebar_title_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'left' => false,
                'right' => false,
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top' => '0px',
                    'padding-bottom' => '10px',
                ),
                'output' => array('.drubo-sidebar .widgetheading')
            ),
            array(
                'id'             => 'sidebar_title_margins',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'left' => false,
                'right' => false,
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'margin-top' => '0px',
                    'margin-bottom' => '10px',
                ),
                'output' => array('.drubo-sidebar .widgetheading')
            ),

        )
    ));

    // Footer Section
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Footer', 'drubo' ),
        'id'               => 'footer',
        'icon'             => 'el el-arrow-down',
    ));

    /**
     * Footer top
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Footer Top', 'drubo' ),
        'id'               => 'footer-top',
        'desc'             => esc_html__( 'These are really basic fields!', 'drubo' ),
        'customizer_width' => '400px',
        'subsection' => true,
        'fields' => array(


            array(
                'id'       => 'enable_footer_widget_area',
                'type'     => 'switch', 
                'title'    => esc_html__('Enable Footer widget area', 'drubo'),
                'on'       => esc_html__('Enable','drubo'),
                'off'       => esc_html__('Disable','drubo'),
                'default'  => true,
            ),

            /**
             * Footer layout
             */
            array(
                'id' => 'footer-sidebar-layout',
                'type' => 'image_select',
                'title' => esc_html__('Footer sidebar layout' , 'drubo'),
                'required' => array('enable_footer_widget_area','equals','1'),
                //
                   'options'  => array(
                        '1'  => array(
                            'alt'   => '1 Column', 
                            'img'   => get_template_directory_uri() . '/inc/footer-layout-img/fs-1.png'
                        ),
                        '2'  => array(
                            'alt'   => '1 Column', 
                            'img'   => get_template_directory_uri() . '/inc/footer-layout-img/fs-2.png'
                        ),
                        '3'  => array(
                            'alt'   => '1 Column', 
                            'img'   => get_template_directory_uri() . '/inc/footer-layout-img/fs-3.png'
                        ),
                        '4'  => array(
                            'alt'   => '1 Column', 
                            'img'   => get_template_directory_uri() . '/inc/footer-layout-img/fs-4.png'
                        ),

                    ),
                   'default' => '4'


            ), // first
            ///////////////////

            
            array(
                'id'       => 'footer_widget_area_bg',
                'type'     => 'background', 
                'title'    => esc_html__('Background', 'drubo'),
                'required' => array('enable_footer_widget_area','equals','1'),
                'default'  => array(
                    'background-color' => '#303030',
                    'background-repeat' => 'no-repeat',
                    'background-size' => 'inherit',
                    'background-attachment' => 'scroll',
                    'background-position' => 'left top',

                ),
                'output' => array('#footer-widget-area')
            ),

            array(
                'id'       => 'footer_top_border',
                'type'     => 'border',
                'title'    => esc_html__( 'Border', 'drubo' ),
                'required' => array('enable_footer_widget_area','equals','1'),
                'left'     => false,
                'right'     => false,
                'all'      => false,
                'desc'     => esc_html__( 'This is the description field, again good for additional info.', 'drubo' ),
                'output'   => array('#footer-widget-area')
            ),


            array(
                'id'             => 'footer_top_margin',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'left'     => false,
                'right'     => false,
                'units'          => array( 'em', 'px', '%' ), 
                'required' => array('enable_footer_widget_area','equals','1'),
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Margins', 'drubo' ),
                'output'         => array('#footer-widget-area')
            ),


            array(
                'id'             => 'footer_top_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'left'     => false,
                'right'     => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'required' => array('enable_footer_widget_area','equals','1'),
                'default'        => array(
                    'padding-top' => '100px',
                    'padding-bottom' => '100px',
                ),
                'output' => array('#footer-widget-area')
            ),


        ) // fields
    ) );

    /**
     * Footer bottom
     */
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Footer Bottom', 'drubo' ),
        'id'               => 'footer-bottom',
        'subsection' => true,
        'fields' => array(
            array(
                'id'       => 'enable_footer_bottom_area',
                'type'     => 'switch', 
                'title'    => esc_html__('Enable Footer bottom area', 'drubo'),
                'on'       => esc_html__('Enable','drubo'),
                'off'       => esc_html__('Disable','drubo'),
                'default'  => true,
            ),

            
            array(
                'id'       => 'footer_bottom_area_bg',
                'type'     => 'background', 
                'title'    => esc_html__('Background', 'drubo'),
                'default'  => array(
                    'background-color' => '#303030',
                    'background-repeat' => 'no-repeat',
                    'background-size' => 'inherit',
                    'background-attachment' => 'scroll',
                    'background-position' => 'left top',

                ),
                'required' => array('enable_footer_bottom_area' , 'equals' , '1'),
                'output' => array('#footer-bottom')
            ),

            array(
                'id'       => 'footer_bottom_border',
                'type'     => 'border',
                'title'    => esc_html__( 'Border', 'drubo' ),
                'output'   => array( '.site-header' ),
                'left' => false,
                'right' => false,
                'all'      => false,
                'desc'     => esc_html__( 'This is the description field, again good for additional info.', 'drubo' ),
                'default'  => array(
                    'border-style'  => 'solid',
                    'border-top'    => '1px',
                    'border-bottom' => '0px',
                    'border-color'  => '#ddd'
                ),
                'required' => array('enable_footer_bottom_area' , 'equals' , '1'),
                'output' => array('#footer-bottom')
            ),
            array(
                'id'             => 'footer_bottom_margin',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'all'            => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'left' => false,
                'right' => false,
                'display_units' => 'px', 
                'title'          => esc_html__( 'Margins', 'drubo' ),
                'default'        => array(
                    'margin-top'    => '0px',
                    'margin-bottom' => '0px',
                ),
                'required' => array('enable_footer_bottom_area' , 'equals' , '1'),
                'output'    => array('#footer-bottom')
            ),


            array(
                'id'             => 'footer_bottom_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'left' => false,
                'right' => false,
                'display_units' => 'px', 
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top'    => '110px',
                    'padding-bottom' => '95px',
                ),
                'required' => array('enable_footer_bottom_area' , 'equals' , '1'),
                'output'    => array('#footer-bottom')
            ),

            array(
                'id'      => 'footer-copyright-text',
                'type'    => 'editor',
                'title'   => esc_html__( 'Footer copyright text', 'drubo' ),
                'default' => '<p><a href="https://hastech.company/#">HasTech</a>'.date('Y').'. All Rights Reserved.</p>',
                'args'    => array(
                    'media_buttons' => true,
                    'textarea_rows' => 5,
                    'teeny'         => false,
                    //'tinymce' => array(),
                    'quicktags'     => false,
                ),
                'required' => array('enable_footer_bottom_area' , 'equals' , '1')
            ),

        ) // fields
    ) );


    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Footer Typography', 'drubo' ),
        'id'               => 'footer_typo',
        'customizer_width' => '400px',
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'widget_typography_title',
                'title' => esc_html__('Widget Title','drubo'),
                'indent' => true,
                'type' => 'section'
            ),
            array(
                'id' => 'footer_widget_title_color',
                'title' => esc_html__('Color','drubo'),
                'type' => 'color',
                'default' => '#FFFFFF',
                'output' => array('color' => '.footer-widget-area .single-footer-widget > h5')
            ),
            array(
                'id'             => 'footer_widget_font_size',
                'type'           => 'dimensions',
                'units'          => array( 'em', 'px', '%' ),    // You can specify a unit value. Possible: px, em, %
                'units_extended' => 'true',  // Allow users to select any type of unit
                'title'          => esc_html__( 'Font Size', 'drubo' ),
                'height'         => false,
                'default'        => array(
                    'width'  => '18px',
                )
            ),
            array(
                'id'             => 'footer_widget_title_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-bottom' => '15px',
                    'padding-top' => '0px',
                ),
                'output' => array('.footer-widget-area .single-footer-widget > h5')
            ),


            array(
                'id' => 'footer_text_typography_title',
                'title' => esc_html__('Footer Text','drubo'),
                'indent' => true,
                'type' => 'section'
            ),
            array(
                'id' => 'footer_text_color',
                'title' => esc_html__('Text color','drubo'),
                'type' => 'color',
                'default' => '#777',
                'output' => array('.footer-area-wrapper' , '.footer-area-wrapper p')
            ),
            array(
                'id' => 'footer_text_link_color',
                'title' => esc_html__('Link color','drubo'),
                'type' => 'color',
                'default' => '#FFF',
                'output' => array('color' => '.footer-area-wrapper a')
            ),
            array(
                'id' => 'footer_text_link_hover_color',
                'title' => esc_html__('Link hover color','drubo'),
                'type' => 'color',
                'default' => '#45C0C8',
                'output' => array('.footer-area-wrapper a:hover')
            ),


            array(
                'id' => 'footer_social_icons_typo',
                'title' => esc_html__('Social Icons','drubo'),
                'type' => 'section',
                'indent' => true
            ),

            array(
                'id' => 'footer_social_icon_color',
                'title' => esc_html__('Icon color','drubo'),
                'type' => 'color',
                'default' => '#FFF',
                'output'  => array(
                    'border-color' => '.footer-area .social-rotate ul li a',
                    'color' => '.footer-area .social-rotate ul li a',
                )
            ),
            array(
                'id' => 'footer_social_icon_hover_color',
                'title' => esc_html__('Icon hover color','drubo'),
                'type' => 'color',
                'default' => '#45C0C8',
                'output' => array(
                    'background-color' => '.footer-area .social-rotate ul li a:hover',
                    'border-color' => '.footer-area .social-rotate ul li a:hover',
                )
            ),
            array(
                'id'             => 'footer_social_icon_paddings',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'all'            => false,
                'left' => false,
                'right' => false,
                'units'          => array( 'em', 'px', '%' ), 
                'units_extended' => 'true',
                'display_units' => 'px',
                'title'          => esc_html__( 'Paddings', 'drubo' ),
                'default'        => array(
                    'padding-top' => '0px',
                    'padding-bottom' => '0px',
                ),
                'output' => array('.footer-area .social-rotate')
            ),

        )
    )
    );

// Custom code

    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom Code', 'drubo' ),
        'id'         => 'editor-ace',
        'icon'  => 'el el-css',
        'fields'     => array(
            array(
                'id'       => 'drubo_custom_css',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'CSS Code', 'drubo' ),
                'subtitle' => esc_html__( 'Paste your CSS code here.', 'drubo' ),
                'desc'     => esc_html__('

CSS Code
Enter your CSS code in the field below. Do not include any tags or HTML in the field. Custom CSS entered here will override the theme CSS. In some cases, the !important tag may be needed. Don\'t URL encode image or svg paths. Contents of this field will be auto encoded.','drubo'),
                'mode'     => 'css',
                'theme'    => 'monokai',
            ),
            array(
                'id'       => 'drubo_custom_js',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'JS Code', 'drubo' ),
                'subtitle' => esc_html__( 'Paste your JS code here.', 'drubo' ),
                'mode'     => 'javascript',
                'theme'    => 'chrome',
                'default'  => "jQuery(document).ready(function(){\n\n});"
            ),
            array(
                'id'       => 'code_before_head',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Space before &lt;/head&gt;', 'drubo' ),
                'desc' => esc_html__( 'Only accepts javascript code wrapped with &lt;script&gt; tags and HTML markup that is valid inside the &lt;/head&gt; tag.', 'drubo' ),
                'mode'     => 'html',
                'theme'    => 'chrome',
            ),
            array(
                'id'       => 'code_before_body',
                'type'     => 'ace_editor',
                'title'    => esc_html__( 'Space before &lt;/body&gt;', 'drubo' ),
                'desc' => esc_html__( 'Only accepts javascript code, wrapped with &lt;script&gt; tags and valid HTML markup inside the &lt;/body&gt; tag.', 'drubo' ),
                'mode'     => 'html',
                'theme'    => 'chrome',
            ),


        )
    ) );




    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Social Media', 'drubo' ),
        'id'               => 'social_media',
        'customizer_width' => '400px',
        'icon'             => 'el el-share-alt',
        'fields' => array(
            array(
                'id'       => 'social_icon_links',
                'type'     => 'sortable',
                'label'    => true,
                'options'  => array(
                    'Twitter'   => '',
                    'Facebook'   => '',
                    'Google+'   => '',
                    'Pinterest'   => '',
                    'Linkedin'   => '',
                    'Github'   => '',
                    'Instagram'   => '',
                    'Medium'   => '',
                    'Vine'   => '',
                    'Tumblr'   => '',
                    'Youtube'   => '',
                )

        ))
    ));


    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $field['msg']    = 'your custom error message';
                $return['error'] = $field;
            }

            if ( $warning == true ) {
                $field['msg']      = 'your custom warning message';
                $return['warning'] = $field;
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'drubo' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'drubo' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }