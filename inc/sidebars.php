<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function drubo_widgets_init() {
	for ($i=1; $i <= 4 ; $i++) { 
		register_sidebar( array(
			'name'          => 'Footer ' . $i ,
			'id'            => 'footer-' . $i,
			'description'   => esc_html__( 'Drag a widget here.', 'drubo' ),
			'before_widget' => '<aside id="%1$s" class="single-footer-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widgetheading">',
			'after_title'   => '</h5>',
		) );
	}
	register_sidebar(array(
	  'id' => 'right-sidebar',
	  'name' => esc_html__( 'Right Sidebar', 'drubo' ),
	  'description' => esc_html__( 'Drag widgets to this sidebar container.', 'drubo' ),
	  'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h5 class="widgetheading">',
	  'after_title' => '</h5>',
	));
	register_sidebar(array(
	  'id' => 'left-sidebar',
	  'name' => esc_html__( 'Left Sidebar', 'drubo' ),
	  'description' => esc_html__( 'Drag widgets to this sidebar container.', 'drubo' ),
	  'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h5 class="widgetheading">',
	  'after_title' => '</h5>',
	));
}
add_action( 'widgets_init', 'drubo_widgets_init' );