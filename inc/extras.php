<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Drubo
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function drubo_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'drubo_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function drubo_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'drubo_pingback_header' );

function drubo_logo(){
	global $drubo_theme_options;
	$link = home_url( '/' );
	echo '<a href="'.$link.'">';
	$logo = $drubo_theme_options['site_logo']['url'];
	$title = $drubo_theme_options['website_title'];
	if($drubo_theme_options['website_logo_or_title'] == 1){
		echo '<img src="'.$logo.'"/>';
	}else{
		echo '<h2 class="text-logo">'.$title.'</h2>';
	}
	echo '</a>';
	
	if(!$drubo_theme_options['site_logo']){ ?>
		<a href="<?php echo esc_url( get_home_url() ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/1.png"/></a>
	<?php }
	
}


function drubo_fb_menu(){
	if(is_user_logged_in())
		echo '<ul><li><a href="'.admin_url('nav-menus.php').'">' . esc_html__( 'Create Menu' , 'drubo' ) . '</a></ul>';
	else
		echo '<ul><li><a href="'.home_url().'">' . esc_html__( 'Home' , 'drubo' ) . ' </a></ul>';
}
function drubo_main_menu(){
	$args = array(
		'theme_location' => 'primary',
		'fallback_cb' => 'drubo_fb_menu',
	);
	wp_nav_menu( $args );
}
function drubo_mobile_menu(){
	$args = array(
		'theme_location' => 'primary',
		'menu_id' => 'nav',
	);
	wp_nav_menu( $args );
}



add_action('wp_head' , function(){
	global $drubo_theme_options;
 ?>
 
<style>
/**
 * Widget title
 */
.footer-widget-area .single-footer-widget > h5 {
    font-size: <?php echo $drubo_theme_options['footer_widget_font_size']['width']; ?>;
    border-bottom: none;
}

.main-menu ul li > ul {
    width: <?php echo $drubo_theme_options['menubar_dropdown_width']; ?>px;
}

/**
 * Page outer bg
 */
<?php if($drubo_theme_options['use_predefine_patterns'] == 1): // when predefine bg pattern is selected ?>
body{
	background-image: url(<?php echo get_template_directory_uri() . '/assets/images/pattern/'.$drubo_theme_options['main_content_outer_bg_image'].'.png'; ?>);
}
<?php else: ?>
body {
	background-color: <?php echo $drubo_theme_options['main_content_outer_bg']['background-color']; ?>;
	background-repeat: <?php echo $drubo_theme_options['main_content_outer_bg']['background-repeat']; ?>;
	background-size: <?php echo $drubo_theme_options['main_content_outer_bg']['background-size']; ?>;
	background-attachment: <?php echo $drubo_theme_options['main_content_outer_bg']['background-attachment']; ?>;
	background-position: <?php echo $drubo_theme_options['main_content_outer_bg']['background-position']; ?>;
	<?php if(!empty($drubo_theme_options['main_content_outer_bg']['background-image'])):; ?>
	background-image: url(<?php echo $drubo_theme_options['main_content_outer_bg']['background-image']; ?>);
	<?php endif; ?>
}
<?php endif; ?>
</style>
	
<?php	} , 999);