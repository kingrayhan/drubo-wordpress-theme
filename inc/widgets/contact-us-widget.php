<?php
class Drubo_contact_us_widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'Drubo_contact_us_widget',
			'description' => 'My Widget is awesome',
		);
		parent::__construct( 'Drubo_contact_us_widget', 'DRUBO: Contact Us', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) { ?>
		<div class="widget footer-menu">
			<?php echo $args['before_widget'].$args['before_title'].$instance['title'].$args['after_title']; ?>
			<div class="contact-details text-white">
				<ul>
				<?php if ($instance['location']): ?>
					<li>
						<a href="#"><i class="zmdi zmdi-pin"></i></a>
						<p><?php echo $instance['location']; ?></p>
					</li>
				<?php endif ?>

				<?php if ($instance['email']): ?>
					<li>
						<a href="#"><i class="zmdi zmdi-email"></i></a>
						<p style="margin-top: 12px;"><?php echo $instance['email']; ?></p>
					</li>
				<?php endif ?>

				<?php if ($instance['phone_number']): ?>
					<li>
						<a href="#"><i class="zmdi zmdi-phone"></i></a>
						<p><?php echo $instance['phone_number']; ?></p>
					</li>
				<?php endif ?>


				</ul>
			</div>
			<?php echo $args['after_widget'] ?>
		</div>
	<?php }

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) { ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo esc_html__('Title:','drubo') ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?php echo isset($instance['title']) ? $instance['title'] : ''; ?>" class="widefat">
		</p>


		<p>
			<label for="<?php echo $this->get_field_id('location'); ?>"><?php echo esc_html__('Location:','drubo') ?></label>
			<input type="text" name="<?php echo $this->get_field_name('location'); ?>" id="<?php echo $this->get_field_id('location'); ?>" value="<?php echo isset($instance['location']) ? $instance['location'] : ''; ?>" class="widefat">
		</p>


		<p>
			<label for="<?php echo $this->get_field_id('email'); ?>"><?php echo esc_html__('Email:','drubo') ?></label>
			<input type="text" name="<?php echo $this->get_field_name('email'); ?>" id="<?php echo $this->get_field_id('email'); ?>" value="<?php echo isset($instance['email']) ? $instance['email'] : ''; ?>" class="widefat">
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('phone_number'); ?>"><?php echo esc_html__('Phone Number(s):','drubo') ?></label>
			<input type="text" name="<?php echo $this->get_field_name('phone_number'); ?>" id="<?php echo $this->get_field_id('phone_number'); ?>" value="<?php echo isset($instance['phone_number']) ? $instance['phone_number'] : ''; ?>" class="widefat">
		</p>
	<?php }
}

add_action( 'widgets_init', function(){
	register_widget( 'Drubo_about_us_widget' );
	register_widget( 'Drubo_contact_us_widget' );
});


