<?php

/**
 * Drubo About Us widget
 */


class Drubo_about_us_widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'drubo_about_us_widget',
			'description' => 'My Widget is awesome',
		);
		parent::__construct( 'drubo_about_us_widget', 'DRUBO: About Us', $widget_ops );
		
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'].$args['before_title'].$instance['title'].$args['after_title']; ?>
	
		<?php if (isset($instance['image_uri'])): ?>
			<img src="<?php echo $instance['image_uri']; ?>" class="about-us-widget-logo">
		<?php endif ?>
			
		<?php if (isset($instance['about_text'])): ?>
			<p><?php echo $instance['about_text']; ?></p>
		<?php endif ?>
	<?php	
		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) { 

	?>
		
	<p>
		<label for="<?php echo $this->get_field_id('title') ?>">Title</label>
		<input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" class="widefat" value="<?php echo isset($instance['title']) ? $instance['title'] : '' ?>">
	</p>

    <p>
        <label for="<?php echo $this->get_field_id('image_uri'); ?>"><?php echo esc_html__('Company Logo','drubo') ?></label><br />

        <?php 
        	if(isset($instance['image_uri'])){
        		echo "<img style='max-width:100%;' src='".$instance['image_uri']."'>";
        	}

        ?>

        <input type="text" class="widefat custom_media_url" name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>" value="<?php echo isset($instance['image_uri']) ? $instance['image_uri'] : ''; ?>" style="margin-top:5px;">

        <input type="button" class="button button-primary custom_media_button" id="custom_media_button" name="<?php echo $this->get_field_name('image_uri'); ?>" value="Upload Image" style="margin-top:5px;" />
    </p>



	<p>
		<label for="<?php echo $this->get_field_id('about_text') ?>"><?php echo esc_html__('Description','drubo') ?></label>
		<textarea name="<?php echo $this->get_field_name('about_text') ?>" id="<?php echo $this->get_field_id('about_text') ?>" cols="30" rows="10" class="widefat"><?php echo isset($instance['about_text']) ? $instance['about_text'] : ''; ?></textarea>
	</p>

	<?php }
}


add_action( 'widgets_init', function(){
	register_widget( 'Drubo_about_us_widget' );
});


// add admin scripts
add_action('admin_enqueue_scripts', 'ctup_wdscript');
function ctup_wdscript() {
    wp_enqueue_media();
    wp_enqueue_script('ads_script', get_template_directory_uri() . '/assets/js/widget.js', false, '1.0', true);
}