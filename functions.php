<?php
/**
 * Drubo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Drubo
 */

if ( ! function_exists( 'drubo_setup' ) ) :



/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function drubo_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Drubo, use a find and replace
	 * to change 'drubo' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'drubo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'blog-fet', 800, 600, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'drubo' ),
		'LeftMenu' => esc_html__( 'Topbar Left Menu', 'drubo' ),
		'RightMenu' => esc_html__( 'Topbar Right Menu', 'drubo' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );	
}
endif;
add_action( 'after_setup_theme', 'drubo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function drubo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'drubo_content_width', 640 );
}
add_action( 'after_setup_theme', 'drubo_content_width', 0 );

/**
 * Change wp post .sticky class
 */
function remove_sticky_class($classes) {
$classes = array_diff($classes, array("sticky"));
$classes[] = 'wordpress-sticky';
return $classes;
}
add_filter('post_class','remove_sticky_class');

/**
 * Enqueue scripts and styles.
 */
function drubo_scripts() {
	/**
	 * ===================================================================================
	 * 	*****	Stylesheets assets  *****
	 * ===================================================================================
	 */
	
	/**
	* Enqueue a CSS stylesheet.
	*
	* Registers the style if source provided (does NOT overwrite) and enqueues.
	*
	*
	* @param string           $handle Name of the stylesheet. Should be unique.
	* @param string           $src    Full URL of the stylesheet, or path of the stylesheet relative to the WordPress root directory.
	*                                 Default empty.
	* @param array            $deps   Optional. An array of registered stylesheet handles this stylesheet depends on. Default empty array.
	* @param string|bool|null $ver    Optional. String specifying stylesheet version number, if it has one, which is added to the URL
	*                                 as a query string for cache busting purposes. If version is set to false, a version
	*                                 number is automatically added equal to current installed WordPress version.
	*                                 If set to null, no version is added.
	* @param string           $media  Optional. The media for which this stylesheet has been defined.
	*                                 Default 'all'. Accepts media types like 'all', 'print' and 'screen', or media queries like
	*                                 '(orientation: portrait)' and '(max-width: 640px)'
	*                                 
	* @link https://codex.wordpress.org/Function_Reference/wp_enqueue_style
	* 
	* wp_enqueue_style( $handle, $src = '', $deps = array(), $ver = false, $media = 'all' )
	*/ 
	// Bootstrap
	wp_enqueue_style( 'Bootstrap' , get_template_directory_uri().'/assets/css/bootstrap.min.css', '' , '3.3.5' , 'all' );
	
	
	// This core.css file contents all plugings css file
	wp_enqueue_style( 'fa' , get_template_directory_uri() . '/assets/css/font-awesome.min.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'animate' , get_template_directory_uri() . '/assets/css/plugins/animate.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'slick' , get_template_directory_uri() . '/assets/css/plugins/slick.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'jquery-ui' , get_template_directory_uri() . '/assets/css/plugins/jquery-ui.min.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'jquery-ui' , get_template_directory_uri() . '/assets/css/plugins/jquery-ui.min.css', '' , '1.0.0' , 'all' );
	
	// Shortcodes
	wp_enqueue_style( 'default' , get_template_directory_uri() . '/assets/css/shortcode/default.css', '' , '1.0.0' , 'all' );
	
	
	
	
	
	wp_enqueue_style( 'google-font-roboto' , 'https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900|Montserrat:400,700|Raleway:100,200,300,400,400i,500,600,700,800,900' );


	// Theme shortcodes/elements style
	//wp_enqueue_style( 'shortcode' , get_template_directory_uri().'/assets/css/shortcode/shortcodes.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'shortcode-header' , get_template_directory_uri().'/assets/css/shortcode/header.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'shortcode-slider' , get_template_directory_uri().'/assets/css/shortcode/slider.css', '' , '1.0.0' , 'all' );
	wp_enqueue_style( 'shortcode-footer' , get_template_directory_uri().'/assets/css/shortcode/footer.css', '' , '1.0.0' , 'all' );
	
	
	
	// Nivo Slider
	wp_enqueue_style( 'nivo-slider' , get_template_directory_uri().'/assets/css/nivo-slider.css', '' , '3.2' , 'all' );
	// Magnific Popup Css
	wp_enqueue_style( 'magnific-popup' , get_template_directory_uri().'/assets/css/magnific-popup.css', '' , '1.0.0' , 'all' );
	// Material design iconic font Css
	wp_enqueue_style( 'material-design-iconic-font' , get_template_directory_uri().'/assets/css/material-design-iconic-font.min.css', '' , '1.0.0' , 'all' );
	// Venubox Css
	wp_enqueue_style( 'venobox' , get_template_directory_uri().'/assets/css/venobox.css', '' , '1.0.0' , 'all' );
	// Owl carsoul Css
	wp_enqueue_style( 'owl-carousel' , get_template_directory_uri().'/assets/css/plugins/owl.carousel.css', '' , '1.0.0' , 'all' );
	// Mean Menu Css
	wp_enqueue_style( 'meanmenu' , get_template_directory_uri().'/assets/css/plugins/meanmenu.min.css', '' , '1.0.0' , 'all' );
	// Mean Menu Css
	// Load our main stylesheet.
	wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
	// Responsive css
	wp_enqueue_style( 'responsive' , get_template_directory_uri().'/assets/css/responsive.css', '' , '1.0.0' , 'all' );
	// Custom css
	wp_enqueue_style( 'custom' , get_template_directory_uri().'/assets/css/custom.css', '' , '1.0.0' , 'all' );
	
	/**
	 * ===================================================================================
	 * 			Javascripts assets
	 * ===================================================================================
	 */
    /**
	 * Enqueue scripts
	 * 
	 * @param     string        $handle      Script name
	 * @param     string        $src         Script url
	 * @param     array         $deps        (optional) Array of script names on which this script depends
	 * @param     string|bool   $ver         (optional) Script version (used for cache busting), set to null to disable
	 * @param     bool          $in_footer   (optional) Whether to enqueue the script before </head> or before </body>
	 *
	 * @link https://codex.wordpress.org/Function_Reference/wp_enqueue_script
	 *
	 * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false )
	 * 
	 */
	
    wp_enqueue_script( 'bootstrap-js' , get_template_directory_uri(). '/assets/js/bootstrap.min.js', array('jquery') , '3.3.5' , true );
    wp_enqueue_script( 'plugins-js' , get_template_directory_uri(). '/assets/js/plugins.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'nivo-slider-js' , get_template_directory_uri(). '/assets/js/jquery.nivo.slider.pack.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'magnific-popup-js' , get_template_directory_uri(). '/assets/js/jquery.magnific-popup.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'isotope-js' , get_template_directory_uri(). '/assets/js/isotope.pkgd.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'venobox-js' , get_template_directory_uri(). '/assets/js/venobox.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'ajax-mail-js' , get_template_directory_uri(). '/assets/js/ajax-mail.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'ajaxchimp-js' , get_template_directory_uri(). '/assets/js/jquery.ajaxchimp.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'owl-carousel-js' , get_template_directory_uri(). '/assets/js/owl.carousel.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'meanmenu-js' , get_template_directory_uri(). '/assets/js/jquery.meanmenu.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'imagesloaded-js' , get_template_directory_uri(). '/assets/js/imagesloaded.pkgd.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'counterup-js' , get_template_directory_uri(). '/assets/js/jquery.counterup.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'waypoints-js' , get_template_directory_uri(). '/assets/js/jquery.waypoints.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'headroom-js' , get_template_directory_uri(). '/assets/js/headroom.min.js', array('jquery') , '1.0.0' , true );
    wp_enqueue_script( 'theme-main-js' , get_template_directory_uri(). '/assets/js/main.js', array('jquery') , '1.0.0' , true );
	
	global $drubo_theme_options;
	if(!empty($drubo_theme_options['drubo_custom_css'])) wp_add_inline_style( 'custom' , $drubo_theme_options['drubo_custom_css'] );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	

	
	
}
add_action( 'wp_enqueue_scripts', 'drubo_scripts' );




if(is_admin()){
	add_action('admin_head',function(){ ?>
<style>
	#drubo_theme_options-main_content_outer_bg_image .redux-image-select img{
		height: 100px !important;
		width: 100px !important;
	}
</style>
<?php });
}



/**
 * One click demo install
 */
require get_template_directory() . '/inc/one-click-demo-install.php';


/**
 * Required plugins
 */
require get_template_directory() . '/inc/class.tgm-activation.php';
require get_template_directory() . '/inc/required-plugins.php';

/**
 * Sidebars
 */
require get_template_directory() . '/inc/sidebars.php';

/**
 * Custom widgets with theme
 */
require get_template_directory() . '/inc/widgets/init.php';


/**
 * Redux Framework Theme Options config
 */
require get_template_directory() . '/inc/theme-options-config.php';

/**
 * CMB2 metabox
 */
if(get_template_directory() . '/inc/drubo-metaboxes.php'){
	require get_template_directory() . '/inc/drubo-metaboxes.php';
}


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';