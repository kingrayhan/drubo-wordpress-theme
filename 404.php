<?php get_header();?>
		<!-- Drubo Story Start -->
		<section class="page-content gray-bg ptb-100">
			<div class="container">
				<div class="row">

							<div id="primary" class="content-area">
								<main id="main" class="site-main" role="main">
									<div style="text-align: center;">
										<img src="<?php echo get_template_directory_uri() . '/assets/images/404.gif' ?>" alt="">
									</div>
								</main><!-- #main -->
							</div><!-- #primary -->
				</div>
			</div>
		</section>
		<!-- Drubo Story End -->
        <!-- Start footer area -->
<?php get_footer(); ?>