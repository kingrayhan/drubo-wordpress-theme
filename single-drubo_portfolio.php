<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Drubo
 */
get_header(); ?>

<!-- Page Area Start -->
<section class="page-content-area gray-bg ptb-100">
	<?php while ( have_posts() ) : the_post(); ?>
	
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="portfolio-details-content white-bg">
						<div class="portfolio-titel ptb-30 text-center text-uppercase">
							<h4><?php the_title(); ?></h4>
						</div>
					</div>
				</div>
			</div>
			<div class="row"> 
				<?php the_content(); ?>
			</div>
		</div>		
		
	<?php endwhile; ?>
</section>
<!-- Page Area End -->

<?php get_footer();
