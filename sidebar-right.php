<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Drubo
 */
global $drubo_theme_options;
?>
<aside class="right-sidebar drubo-sidebar">
<?php 
	if (is_archive())
		dynamic_sidebar($drubo_theme_options['select_arc_right_sidebar']);
	else if(is_page())
		dynamic_sidebar($drubo_theme_options['select_page_right_sidebar']);
	else if (is_single())
		dynamic_sidebar($drubo_theme_options['select_post_right_sidebar']);
	else if (drubo_is_blog())
		dynamic_sidebar($drubo_theme_options['select_blog_right_sidebar']);
	else if (is_search())
		dynamic_sidebar($drubo_theme_options['select_search_right_sidebar']);
	
	if(empty($drubo_theme_options))
		dynamic_sidebar('right-sidebar');

?>
</aside>
