<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Drubo
 */
global $drubo_theme_options;

if(!empty($drubo_theme_options)):
$footer_sidebar_count = $drubo_theme_options['footer-sidebar-layout'];
$footer_sidebar_class = ceil(12/$footer_sidebar_count);
$enable_footer_widget_area = $drubo_theme_options['enable_footer_widget_area'];
endif;
?>
<div class="footer-area-wrapper">
<?php if(isset($enable_footer_widget_area)): ?>
<div class="footer-widget-area" id="footer-widget-area">
    <div class="container">
        <div class="row">
		<?php for ($i=1; $i <= $footer_sidebar_count; $i++) { ?>
				<div class="col-md-<?php echo $footer_sidebar_class; ?> col-xs-12 res-pb-xs-30">
					<?php dynamic_sidebar('footer-'.$i); ?>
				</div>
		<?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if(empty($drubo_theme_options)): ?>
<div class="footer-area ptb-100 black-half-bg">
    <div class="container">
        <div class="row">
		<?php for ($i=1; $i <= 4; $i++) { ?>
				<div class="col-md-3 col-xs-12 res-pb-xs-30">
					<?php dynamic_sidebar('footer-'.$i); ?>
				</div>
		<?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>



<?php if($drubo_theme_options['enable_footer_bottom_area']): ?>
        <footer class="footer-area" id="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="footer-content text-center">
							<?php if($drubo_theme_options['enable_footer_bottom_social_icons']): ?>
							<div class="social-rotate">
								<ul>
									<?php social_links_li(); ?>
								</ul>
							</div>
							<?php endif; ?>
							<!-- Copyright Footer -->
							<div class="fotter-copyright text-white mt-60">
								<?php  
									if(!empty($drubo_theme_options['footer-copyright-text'])){
										echo $drubo_theme_options['footer-copyright-text'];
									}
								?>
							</div>
							<!-- Copyright Footer -->
						</div>
					</div>
				</div>
			</div>
		</footer>
<?php endif; ?>
</div><!-- . footer-area-wrapper -->

</div> <!-- .wrapper .gray-bg -->
<?php wp_footer(); ?>
<!-- Before body code -->
<?php echo (!empty($drubo_theme_options['code_before_body'])) ? $drubo_theme_options['code_before_body'] : '' ; ?>
<!-- Custom js code -->
<script>
	<?php echo $drubo_theme_options['drubo_custom_js']; ?>
</script>
</body>
</html>