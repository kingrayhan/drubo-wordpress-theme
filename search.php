<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Drubo
 */
?>
<?php get_header(); global $drubo_theme_options; ?>

<section class="page-content ptb-100">
	<div class="container">
		<div class="row">
			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'drubo' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php echo get_template_part('template-parts/'.$drubo_theme_options['select_search_sidebar_layout']); 
				if(empty($drubo_theme_options)) echo get_template_part('template-parts/blog-right-sidebar'); ?>


		</div>
	</div>
</section>

<?php get_footer(); ?>