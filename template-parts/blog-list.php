    <?php global $drubo_theme_options; ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-image">
            <div class="post-heading">
                <h3>
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h3>

                </div> 
                    <?php the_post_thumbnail('blog-fet'); ?>
                </div>

                <p><?php 
                    if($drubo_theme_options['blog_content_display'] == 'excerpt')
                        echo wp_trim_words( get_the_content() , $drubo_theme_options['blog_excerpt_length'] );
                    else if($drubo_theme_options['blog_content_display'] == 'Full_Content') 
                        the_content();
                    else
                        echo wp_trim_words( get_the_content() , 55 );
                ?></p>

<div class="bottom-article">
    <ul class="meta-post">

        <?php if($drubo_theme_options['show_blog_date']): ?><li><i class="fa fa-calendar"></i><?php the_time( $drubo_theme_options['show_blog_date_format'] ); ?></li><?php endif; ?>

        <?php if($drubo_theme_options['show_blog_author']): ?><li><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></li><?php endif; ?>

        <?php if($drubo_theme_options['show_blog_comment_count']): ?><li><i class="fa fa-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number( $drubo_theme_options['blog_no_comment_text'] , $drubo_theme_options['blog_one_comment_text'] , $drubo_theme_options['blog_n_comment_text'] );?></a></li><?php endif; ?>

    </ul>
    
    <?php if(empty($drubo_theme_options['show_blog_date'])):?>
        <ul class="meta-post">
    
            <li><i class="fa fa-calendar"></i><?php the_time( $drubo_theme_options['show_blog_date_format'] ); ?></li>
    
            <li><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></li>
    
           <li><i class="fa fa-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number( esc_html__('No comment' , 'drubo') , esc_html__('1 Comment','drubo') , esc_html__('% Comment','drubo') );?></a></li>
    
        </ul>
        <a href="<?php echo esc_url( get_permalink() ); ?>" class="pull-right"><?php echo esc_html__('Read More' , 'drubo'); ?><i class="icon-angle-right"></i></a>
    <?php endif; ?>
    

     <?php if($drubo_theme_options['show_blog_readmore']): ?><a href="<?php echo esc_url( get_permalink() ); ?>" class="pull-right"><?php echo $drubo_theme_options['blog_read_more_txt']; ?><i class="icon-angle-right"></i></a><?php endif; ?>

</div>
    </article>