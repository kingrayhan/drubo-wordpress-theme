<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
			<div class="col-lg-8">
					<?php
					if ( have_posts() ) : ?>
					<?php 
						while ( have_posts() ) : the_post();
							
							
							get_template_part( 'template-parts/blog-list' );


						endwhile;

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>



					<?php drubo_pagination(); ?>

			</div>
			<div class="col-lg-4">
				<?php get_sidebar('right'); ?>
			</div>

	</main><!-- #main -->
</div><!-- #primary -->