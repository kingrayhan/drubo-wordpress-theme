<div class="container">
	<div class="row">
		<div class="col-md-8">
			
			<?php title_and_meta(); ?>
			
			<div id="primary" class="content-area" style="padding:0;">
				<main id="main" class="site-main" role="main">
					<?php
					if ( have_posts() ) : ?>
					<?php 
						while ( have_posts() ) : the_post(); ?>						
							
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-content">
								<?php
									the_content( sprintf(
										/* translators: %s: Name of current post. */
										wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'drubo' ), array( 'span' => array( 'class' => array() ) ) ),
										the_title( '<span class="screen-reader-text">"', '"</span>', false )
									) );

									wp_link_pages( array(
										'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'drubo' ),
										'after'  => '</div>',
									) );
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
								?>
							</div><!-- .entry-content -->

							<footer class="entry-footer">
								<?php drubo_entry_footer(); ?>
							</footer><!-- .entry-footer -->
						</article><!-- #post-## -->


					<?php	endwhile;

						the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>
				</main><!-- #main -->
			</div><!-- #primary -->		
		</div>
		<div class="col-lg-4">
			<?php get_sidebar('right'); ?>
		</div>
	</div>
</div>