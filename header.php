<?php global $drubo_theme_options; ?>
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Drubo
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<!-- Before Head Code -->
<?php echo (!empty($drubo_theme_options['code_before_head'])) ? $drubo_theme_options['code_before_head'] : ''; ?>
</head>
<body <?php body_class(); ?>>

<?php
	// Page layout in All page
	$page_layout = ($drubo_theme_options['drubo_page_layout'] == 'boxed') ? 'boxed-layout' : '';
	// Page layout in single po single page
	if(is_single() || is_page() ){
			$prefix = 'Drubo_';
			$page_layout = get_post_meta( get_the_id() , $prefix . 'page_type' , true );
			$page_layout = ($page_layout == 'boxed') ? 'boxed-layout' : '';
	}
?>
<div class="wrapper <?php echo $page_layout; ?>">


<?php 
if(!empty($drubo_theme_options['header_style']))
	echo get_template_part('/inc/menustyle/drubo-header-' . $drubo_theme_options['header_style']); 
else
	echo get_template_part('/inc/menustyle/drubo-header-1'); 
?>