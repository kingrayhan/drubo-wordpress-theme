<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Drubo
 */
global $drubo_theme_options;
?>

<aside class="left-sidebar drubo-sidebar">
	<?php
	if (is_archive())
		dynamic_sidebar($drubo_theme_options['select_arc_left_sidebar']);
	else if(is_page())
		dynamic_sidebar($drubo_theme_options['select_page_left_sidebar']);
	else if (is_single())
		dynamic_sidebar($drubo_theme_options['select_post_left_sidebar']);
	else if (drubo_is_blog())
		dynamic_sidebar($drubo_theme_options['select_blog_left_sidebar']);
	else if (is_search())
		dynamic_sidebar($drubo_theme_options['select_search_left_sidebar']);
		
		
		
	if(empty($drubo_theme_options))
		dynamic_sidebar('left-sidebar');
	?>
</aside>
