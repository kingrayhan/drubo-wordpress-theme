<?php get_header(); global $drubo_theme_options; ?>

<section class="page-content">
	<div class="container">
		<div class="row">

<?php 
if(!empty($drubo_theme_options['select_blog_sidebar_layout'])){
	echo get_template_part('template-parts/'.$drubo_theme_options['select_blog_sidebar_layout']); 
}else{
	echo get_template_part('template-parts/blog-right-sidebar'); 
}

	

	
?>


		</div>
	</div>
</section>

<?php get_footer(); ?>